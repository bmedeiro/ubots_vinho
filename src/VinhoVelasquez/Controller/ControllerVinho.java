/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vinhoVelasquez.Controller;

import VinhoVelasquez.Conexao.ConexaoAPI_Vinho;
import vinhoVelasquez.Model.Historico;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import static javafx.scene.input.KeyCode.T;
import vinhoVelasquez.Model.Cliente;

/**
 *
 * @author Bruno
 */
public class ControllerVinho {

    ConexaoAPI_Vinho csf;
    float resultado = 0;
    Cliente cliente;

    public ControllerVinho() throws Throwable {
        this.csf = new ConexaoAPI_Vinho();
    }

    public ArrayList<Historico> listarClientes() throws Throwable {

        ArrayList<Historico> dados = csf.buscaHistorico();
        ArrayList<Historico> resultado = new ArrayList<Historico>();

        for (int i = 0; i < dados.size(); i++) {

            resultado.add(dados.get(i));

            Collections.sort(resultado, new Comparator() {
                public int compare(Object o1, Object o2) {
                    Historico p1 = (Historico) o1;
                    Historico p2 = (Historico) o2;
                    return p1.valorTotal < p2.valorTotal ? +1 : (p1.valorTotal > p2.valorTotal ? -1 : 0);

                }

            }
            );

        }

        return resultado;
    }

    public ArrayList<Historico> listarMaiorCompra() throws Throwable {

        ArrayList<Historico> dados = csf.buscaHistorico();
        ArrayList<Historico> resultado = new ArrayList<Historico>();

        for (int i = 0; i < dados.size(); i++) {

            if (dados.get(i).data.contains("2016")) {
                resultado.add(dados.get(i));

            }

            Collections.sort(resultado, new Comparator() {
                public int compare(Object o1, Object o2) {
                    Historico p1 = (Historico) o1;
                    Historico p2 = (Historico) o2;
                    return p1.valorTotal < p2.valorTotal ? +1 : (p1.valorTotal > p2.valorTotal ? -1 : 0);

                }

            }
            );

        }

        return resultado;
    }

    public ArrayList<Cliente> listarFiel() throws Throwable {
        int freq = 0;
        ArrayList<Historico> dados = csf.buscaHistorico();
        HashSet<Cliente> res_temp = new HashSet<>();
        List<String> temp = new ArrayList<>();
        ArrayList<Cliente> resultado;

        for (int i = 0; i < dados.size(); i++) {

            temp.add(dados.get(i).nome);

        }

        for (int a = 0; a < temp.size(); a++) {
            freq = Collections.frequency(temp, dados.get(a).nome);

            Cliente h = new Cliente();
            h.nome = temp.get(a);
            h.quantidade = freq;
            res_temp.add(h);

        }

        resultado = new ArrayList<>(res_temp);

        Collections.sort(resultado, new Comparator() {
            public int compare(Object o1, Object o2) {
                Cliente p1 = (Cliente) o1;
                Cliente p2 = (Cliente) o2;
                return p1.quantidade < p2.quantidade ? +1 : (p1.quantidade > p2.quantidade ? -1 : 0);

            }

        }
        );

        return resultado;
    }

}
