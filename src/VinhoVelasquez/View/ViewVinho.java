package vinhoVelasquez.View;

import vinhoVelasquez.Controller.ControllerVinho;
import vinhoVelasquez.Model.Historico;
import java.util.ArrayList;
import vinhoVelasquez.Model.Cliente;

public class ViewVinho {

    String resultado = null;
    ControllerVinho controller;

    public ViewVinho() throws Throwable {
        this.controller = new ControllerVinho();
    }

    public void verificaComando(String temp) throws Throwable {
        String temp_comando = temp;

        switch (temp_comando) {
            case "comandos":
                System.out.println("\n comandos --  lista de comandos \n 1        --  apresentação da lista de clientes ordenados pelo maior valor de compra"
                        + "\n 2        --  apresentação da maior compra feita em 2016 \n 3        --   apresentação dos clientes mais fiéis ");

               break;

            case "1":

                ArrayList<Historico> resultadoDados = controller.listarClientes();

                System.out.println("LISTA DE CLIENTES POR MAIOR VALOR DE COMPRA " + "\n");

                for (int i = 0; i < resultadoDados.size(); i++) {

                    System.out.println(
                            "Cliente: " + resultadoDados.get(i).nome + "\n"
                            + "Valor: " + resultadoDados.get(i).valorTotal + "\n \n");
                }

                break;

            case "2":
                ArrayList<Historico> resultadoMaiorCompra = controller.listarMaiorCompra();

                System.out.println("MAIOR COMPRA EM 2016 " + "\n");


                    System.out.println(
                            "O(A) cliente " + resultadoMaiorCompra.get(0).nome + " comprou o valor de " + resultadoMaiorCompra.get(0).valorTotal + " reais em 2016" + "\n \n");
                

                break;
                
            case "3":
                ArrayList<Cliente> resultadoMaisFieis = controller.listarFiel();

                System.out.println(resultado = "LISTA DOS CLIENTES MAIS FIÉIS: " + "\n");

                for (int i = 0; i < 3 ; i++) {

                    System.out.println(
                            "O(A) cliente " + resultadoMaisFieis.get(i).nome +  " apareceu " + resultadoMaisFieis.get(i).quantidade + " vezes na sua loja!");
                }
                break;

            case "\\sair":
                System.exit(0);
                break;
        }

    }
}
