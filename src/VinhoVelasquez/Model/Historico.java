/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vinhoVelasquez.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Bruno
 */
public class Historico {

    public String codigo;
    public String nome;
    public String data;
    public String cliente;
    public List itens;
    public float valorTotal;
    int count = 0;

    public Historico() {
    }

 
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }


    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public List getItens() {
        return itens;
    }

    public void setItens(List itens) {
        this.itens = itens;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float ValorTotal) {
        this.valorTotal = ValorTotal;

    }

    public String encontrarNome(String cpf, ArrayList<Cliente> dadosC) { //comparar cpfs e atrbui o nome do cliente

        for (int i = 0; i < dadosC.size(); i++) {

            if (cpf.equals("0" + dadosC.get(i).cpf)) { // if para capturar os 49 clientes têm seus cpfs formatados como "0000.xxx.xxx-xx"
                return dadosC.get(i).nome;

            } else if (cpf.equals(dadosC.get(i).cpf)) {
                return dadosC.get(i).nome;

            }

        }

        return "Nome não encontrado";

    }

    

}
