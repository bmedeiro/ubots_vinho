package VinhoVelasquez.Conexao;

import vinhoVelasquez.Model.Cliente;
import vinhoVelasquez.Model.Historico;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import sun.net.www.protocol.http.HttpURLConnection;

public class ConexaoAPI_Vinho {
    ArrayList<Cliente> dadosCliente = new ArrayList<>();


    public ConexaoAPI_Vinho() throws Throwable {
    this.buscaClientes(); // buscando informações dos clientes

    }

    public ArrayList buscaClientes() throws Throwable {

        Gson gson = new Gson();
        Cliente[] c = null;
        try {
            URL url = new URL("http://www.mocky.io/v2/598b16291100004705515ec5");

            HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();

            httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");

            InputStreamReader reader2 = new InputStreamReader(httpcon.getInputStream());

            c = gson.fromJson(reader2, Cliente[].class);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        for (Cliente cli : c) {
            Cliente dadosTemp = new Cliente();
            dadosTemp.nome = cli.nome;
            dadosTemp.cpf = cli.cpf.replace('-', '.');
            dadosTemp.id = cli.id;
            dadosCliente.add(dadosTemp);
        }
        

        return dadosCliente;

    }

    public ArrayList buscaHistorico() throws Throwable {

        Gson gson = new Gson();
        Historico[] h = null;
        ArrayList<Historico> dadosHistorico = new ArrayList<Historico>();
        Historico historico = new Historico();

        try {
            URL url = new URL("http://www.mocky.io/v2/598b16861100004905515ec7");

            HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();

            httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");

            InputStreamReader reader = new InputStreamReader(httpcon.getInputStream());

            h = gson.fromJson(reader, Historico[].class);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

     
        for (Historico his : h) {
            Historico dadosH = new Historico();
            dadosH.cliente = his.cliente;
            dadosH.valorTotal = his.valorTotal;
            dadosH.data = his.data;
            
            String nomeTemp = historico.encontrarNome(dadosH.cliente, dadosCliente); //comparando cpf para encontrar o nome
            dadosH.nome = nomeTemp;

            dadosHistorico.add(dadosH);

        }
        

        return dadosHistorico;

    }

}
